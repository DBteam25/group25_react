import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import axios from "axios";
import "bootstrap/dist/css/bootstrap.min.css";
import Login from "./Components/Login";

import Header from "./Components/Header";
import Footer from "./Components/Footer";
import AllDatas from "./Components/AllDatas";
import LineChart from "./Components/LineChart";
import BarInstrument from "./Components/BarInstrument";
import {Line} from 'react-chartjs-2';
import StoredData from "./Components/StoredData";
import HorizontalAverage from "./Components/HorizontalAverage";

const DealSURL = `http://localhost:4000/Todos`;
const DealSURL2 = `http://datagen-group-25.apps.dbgrads-6eec.openshiftworkshop.com/rawdeals`;
let data_price_label = ["Astronomica", "Borealis", "Celestial", "Deuteronic", "Eclipse", "Floral", "Galactia", "Heliosphere", 
"Interstella", "Jupiter", "Koronis", "Lunatic"];

let map_sell = Array(12, 10);
let map_buy = Array(12, 10);

const App = () => {
  const [deals, setDeals] = useState(Array(100));
  const [map_buy_stored, setBuy] = useState([]);
  const [onlineStatus, setOnlineStatus] = useState(false);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    getDeals();
  }, []);

  const getDeals= async () => {

    try {
      let source = new EventSource(DealSURL2)
      source.addEventListener('message', function(e) {
        let data = JSON.parse(e.data);
        deals.unshift(data);
        let d = deals.slice(0,99)
        setDeals(d);
        if (deals[0].type == 'S') {
            map_sell[data_price_label.indexOf(deals[0].instrumentName)] = deals[0].__price;
        } else {
            map_buy[data_price_label.indexOf(deals[0].instrumentName)] = deals[0].__price;
        }
        setBuy([map_sell, map_buy]);
      }, false);
      
      source.addEventListener('open', function(e) {
        console.log('stream opened');
      }, false);
      
      source.addEventListener('error', function(e) {
        if (e.readyState == EventSource.CLOSED) {
          console.log('stream closed');
        }
      }, false);
      //setDeals(deals);
      setLoading(false);
      setOnlineStatus(true);
    } catch (e) {
      setDeals(e.message);
      setBuy(e.message);
      setLoading(false);
      setOnlineStatus(false);
    }
  };

  return (
    <Router>
      <div className="container">
        <Header />
        <div className="container">
          {!onlineStatus && !loading ? (
            <h3>The data server may be offline, changes will not be saved</h3>
          ) : null}
          <Route path="/" exact component={Login} />
          <Switch>
            <Route
              path="/real"
              exact
              render={() => <div>
                <h3 align = "center">Realtime data</h3>
                <AllDatas allDeals={deals} loading={loading}/>
                </div>}
            />
            <Route
              path="/line"
              render={() => 
              
                <div>
                <h2 align="center">Graphic: buying/selling price</h2>
                <HorizontalAverage averageStatistic = {map_buy_stored} />
                <LineChart/>
                </div>
            }
            />
            />
            <Route
              path="/statistics"
              exact
              render={() => 
              <div>
                <BarInstrument />
              </div>
            }
            />
            <Route
              path="/store"
              exact
              render={() => <StoredData/>}
            />
            />
          </Switch>
        </div>
        <Footer />
      </div>
    </Router>
  );
};

export default App;
