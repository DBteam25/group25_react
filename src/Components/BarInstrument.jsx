import React from 'react';
import {HorizontalBar} from 'react-chartjs-2';
import "./css/FlexboxContainer.css";
import AverageInstrument from "./AverageInstrument";
import DealerTableItem from "./DealerTableItem";

const Statistic = props => {
  return (
    <tr>
      <td>
        {props.deal.id1}
      </td>
      <td>
        {props.deal.id2}
      </td>
      <td>
        {props.deal.id3}
      </td>
    </tr>
  );
};

class BarInstrument extends React.Component {

  render() {
    let staticticBS = [
        {"instrumentName": "Astronomica",
        "avbuy": 2520,
        "avsell": 2580},
        {"instrumentName": "Borealis",
        "avbuy": 1244,
        "avsell": 1355},
        {"instrumentName": "Celestial",
        "avbuy": 12324,
        "avsell": 21435},
        {"instrumentName": "Deuteronic",
        "avbuy": 133,
        "avsell": 231},
        {"instrumentName": "Eclipse",
        "avbuy": 13,
        "avsell": 188},
        {"instrumentName": "Floral",
        "avbuy": 132435,
        "avsell": 211580},
        {"instrumentName": "Galactia",
        "avbuy": 134,
        "avsell": 5433},
        {"instrumentName": "Heliosphere",
        "avbuy": 423,
        "avsell": 678},
        {"instrumentName": "Interstella",
        "avbuy": 35,
        "avsell": 879},
        {"instrumentName": "Jupiter",
        "avbuy": 645,
        "avsell": 989},
        {"instrumentName": "Koronis",
        "avbuy": 546,
        "avsell": 657},
        {"instrumentName": "Lunatic",
        "avbuy": 6345,
        "avsell": 7678}
    ];
      let data  = staticticBS.map(currentDeal=> (
        <AverageInstrument
          deal={currentDeal}
          key={currentDeal._id}
          selectDeal={this.props.selectDeal}
        />));

        let staticticBS2 = [
          {"dealer": "Lewis",
          "ending": 2520,
          "realised": 2580,
          "effective": 1522},
          {"dealer": "Selvyn",
          "ending": 435,
          "realised": 2454,
          "effective": 2355},
          {"dealer": "Richard",
          "ending": 244,
          "realised": 46534,
          "effective": 53564},
          {"dealer": "Lina",
          "ending": (-6786),
          "realised": -2194,
          "effective": -4155},
          {"dealer": "John",
          "ending": 43543,
          "realised": 53457,
          "effective": 9454},
          {"dealer": "Nidia",
          "ending": 43256,
          "realised": 456557,
          "effective": 49754}
      ];
        let data2  = staticticBS2.map(currentDeal=> (
          <DealerTableItem
            deal={currentDeal}
            key={currentDeal._id}
            selectDeal={this.props.selectDeal}
          />));

    return (
      <div>

      <div className="container">
          <h3 align="center">Average buy and sell</h3>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Instrument</th>
                <th>Average buy</th>
                <th>Average sell</th>
              </tr>
            </thead>
            <tbody>{data}</tbody>
          </table>
        </div>

        <div className="container">
          <h3 align="center">Statistics per dealer</h3>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Dealer</th>
                <th>Ending positions</th>
                <th>Realised profit/loss</th>
                <th>Effective profit/loss</th>
              </tr>
            </thead>
            <tbody>{data2}</tbody>
          </table>
        </div>

    </div>
    );
  }
}

export default BarInstrument;