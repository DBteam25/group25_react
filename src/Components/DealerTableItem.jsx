import React from "react";

const DealerTableItem = props => {
  return (
    <tr>
      <td>
        {props.deal.dealer}
      </td>
      <td>
        {props.deal.ending}
      </td>
      <td>
        {props.deal.realised}
      </td>
      <td>
        {props.deal.effective}
      </td>
    </tr>
  );
};

export default DealerTableItem;
