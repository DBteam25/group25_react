import React from "react";
import { Link, NavLink } from "react-router-dom";

import logo from "./images/qa.jpeg";

const Header = () => {
  return (
    <header>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <Link className="navbar-brand" to="/real">
          The best APP
        </Link>
        <div className="collapse navbar-collapse">
          <ul className="navbar-nav mr-auto">
            <li className="navbar-item">
              <NavLink
                to="/real"
                className="nav-link"
                activeClassName="nav-link active"
              >
                Realtime data
              </NavLink>
            </li>
            <li className="navbar-item">
              <NavLink
                to="/line"
                className="nav-link"
                activeClassName="nav-link active"
              >
                Graphics
              </NavLink>
            </li>
            <li className="navbar-item">
              <NavLink
                to="/statistics"
                className="nav-link"
                activeClassName="nav-link active"
              >
                Statistics
              </NavLink>
            </li>
            <li className="navbar-item">
              <NavLink
                to="/store"
                className="nav-link"
                activeClassName="nav-link active"
              >
                Stored data
              </NavLink>
            </li>
          </ul>
        </div>
      </nav>
    </header>
  );
};

export default Header;
