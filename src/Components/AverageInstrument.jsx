import React from "react";

const AverageInstrument = props => {
  return (
    <tr>
      <td>
        {props.deal.instrumentName}
      </td>
      <td>
        {props.deal.avbuy}
      </td>
      <td>
        {props.deal.avsell}
      </td>
    </tr>
  );
};

export default AverageInstrument;
