import React from "react";

import "./css/AllDatas.css";

import Deal from "./Deal";

class AllDatas extends React.Component {
  render() {
      let deals = [];
      if (this.props.allDeals.length && typeof this.props.allDeals[0] !== "string") {
        deals = this.props.allDeals.map(currentDeal=> (
          <Deal
            deal={currentDeal}
            key={currentDeal._id}
            selectDeal={this.props.selectDeal}
          />
        ));
      } else if (this.props.loading) {
        deals.push(
          <tr key="loading">
            <td colSpan="3">Please wait - retrieving the deals</td>
          </tr>
        );
      } else {
        deals.push(
          <tr key="error">
            <td colSpan="3">There has been an error retrieving the deals</td>
          </tr>
        );
      }
      // console.log(deals);
      return (
        <div className="container">
          
          <table className="table table-striped">
            <thead>
              <tr>
                <th></th>
                <th>Instrument</th>
                <th>Counter party</th>
                <th>Price</th>
                <th>Type</th>
                <th>Quantity</th>
                <th>Time</th>
              </tr>
            </thead>
            <tbody>{deals}</tbody>
          </table>
        </div>
      );
  }

  componentDidMount() {
    this.interval = setInterval(() => this.setState({ time: Date.now() }), 100);
  }
  componentWillUnmount() {
    clearInterval(this.interval);
  }
}

export default AllDatas;
