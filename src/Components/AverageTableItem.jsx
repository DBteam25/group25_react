import React from "react";

const AverageTableItem = props => {
  return (
    <tr>
      <td>
        {props.deal.instrumentName}
      </td>
      <td align="center">
        {props.deal.averageBuy}
      </td>
      <td align="center">
        {props.deal.averageSell}
      </td>
    </tr>
  );
};

export default AverageTableItem;
