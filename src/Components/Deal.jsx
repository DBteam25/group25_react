import React from "react";

const Deal = props => {
  return (
    <tr>
      <td>
        {props.deal.id}
      </td>
      <td>
        {props.deal.instrumentName}
      </td>
      <td>
        {props.deal.cpty}
      </td>
      <td>
        {props.deal.__price}
      </td>
      <td>
        {props.deal.type}
      </td>
      <td>
        {props.deal.quantity}
      </td>
      <td>
        {props.deal.time}
      </td>
    </tr>
  );
};

export default Deal;
