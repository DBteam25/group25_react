import React from 'react';
import {Line, HorizontalBar} from 'react-chartjs-2';
import "./css/LineBlock.css";
import AllDatas from './AllDatas';

const data = {
  labels: ["Astronomica", "Borealis", "Celestial", "Deuteronic", "Eclipse", "Floral", "Galactia", "Heliosphere", 
  "Interstella", "Jupiter", "Koronis", "Lunatic"],
  datasets: [
    {
      label: 'buy',
      backgroundColor: 'rgba(255,99,132,0.2)',
      borderColor: 'rgba(255,99,132,1)',
      borderWidth: 1,
      hoverBackgroundColor: 'rgba(255,99,132,0.4)',
      hoverBorderColor: 'rgba(255,99,132,1)',
      data: [65, 59, 80, 81, 56, 55, 40, 10, 80, 81, 56, 55]
    },
    {
      label: 'sell',
      backgroundColor: 'rgba(1,1,255,0.2)',
      borderColor: 'rgba(1,1,255,1)',
      borderWidth: 1,
      hoverBackgroundColor: 'rgba(1, ,1,255,,0.4)',
      hoverBorderColor: 'rgba(1,1,255,,1)',
      data: [10, 30, 40, 50, 10, 20, 40, 60, 10, 30, 40, 50]
    }
  ]
};

class LineChart extends React.Component {
  constructor(props) {
    super(props);
    this._isMounted = false;
    this.state = this.getState()
  }

  getState = () => ({
    labels: Array.from(Array(100).keys()),
    datasets: [{
          label: 'Buy',
          fill: false,
          lineTension: 0.1,
          backgroundColor: 'rgba(245, 0, 0,0.4)',
          borderColor: 'rgba(245, 0, 0, 1)',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: 'rgba(245, 0, 0,1)',
          pointBackgroundColor: '#fff',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: 'rgba(245, 0, 0,1)',
          pointHoverBorderColor: 'rgba(220,220,220,1)',
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: Array.from({length: 100}, () => Math.floor(Math.random() * 100))
    },
    {
          label: 'Sell',
          fill: false,
          lineTension: 0.1,
          backgroundColor: 'rgba(10,100,192,0.4)',
          borderColor: 'rgba(10,100,192,1)',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: 'rgba(10,100,192,1)',
          pointBackgroundColor: '#fff',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: 'rgba(10,100,192,1)',
          pointHoverBorderColor: 'rgba(220,220,220,1)',
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: Array.from({length: 100}, () => Math.floor(Math.random() * 40))
      }]
  })

	componentWillMount() {
    this._isMounted = true;
		setInterval(() => {
      if (this._isMounted)
			  this.setState(this.getState());
    }, 3000);    
	}

  componentWillUnmount() {
    this._isMounted = false;
 }
  render() {
    return (
      
      <div>
        <h2 align="center">Graphic: buying and selling price</h2>
        <div>
        <div align="center" float= "left" display= "inline-block">
          <div align="center">
              <h7>Select Instrument </h7>
          </div>
          <div>
            <select class="selectpicker" align="center">
              <option>Astronomica</option>
              <option>Borealis</option>
              <option>Celestial</option>
              <option>Deuteronic</option>
              <option>Eclipse</option>
              <option>Floral</option>
              <option>Galactia</option>
              <option>Heliosphere</option>
              <option>Interstella</option>
              <option>Jupiter</option>
              <option>Koronis</option>
              <option>Lunatic</option>
            </select>
          </div>
        </div>


        <div align="center" float= "left" display= "inline-block">
        <div align="center">
            <h7>Select Period</h7>
        </div>
        <div>
          <select class="selectpicker" align="center">
            <option>Current</option>
            <option>1 day</option>
            <option>12 hours</option>
            <option>6 hours</option>
            <option>3 hours</option>
            <option>1 hours</option>
            <option>30 minutes</option>
            <option>15 minutes</option>
            <option>5 minutes</option>
          </select>
        </div>
        </div>
        </div>
        <div className="container">
            <Line data = {this.state}/>
        </div>
        <div class="flexbox-container">
      </div>
      </div>
    );
  }
}

export default LineChart;