import React from 'react';

const Footer = () => {
    return (
        <footer className="bg-light mt-auto py-3">
            <div className="container text-center">
                <span className="text-muted ">&copy; DataShtruden</span>
            </div>
        </footer>
    );
};

export default Footer;
