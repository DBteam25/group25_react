import React from 'react';
import {Line, HorizontalBar} from 'react-chartjs-2';
import "./css/LineBlock.css";
import AllDatas from './AllDatas';

class HorizontalAverage extends React.Component {
  constructor(props) {
    super(props);
    this._isMounted = false;
    this.state = this.getState()
  }

  getState = () => ({
    labels: ["Astronomica", "Borealis", "Celestial", "Deuteronic", "Eclipse", "Floral", "Galactia", "Heliosphere", 
    "Interstella", "Jupiter", "Koronis", "Lunatic"],
    datasets: [
      {
        label: 'buy',
        backgroundColor: 'rgba(255,99,132,0.2)',
        borderColor: 'rgba(255,99,132,1)',
        borderWidth: 1,
        hoverBackgroundColor: 'rgba(255,99,132,0.4)',
        hoverBorderColor: 'rgba(255,99,132,1)',
        data: this.props.averageStatistic[0]
      },
      {
        label: 'sell',
        backgroundColor: 'rgba(0,0,254,0.2)',
        borderColor: 'rgba(0,0,254,1)',
        borderWidth: 1,
        hoverBackgroundColor: 'rgba(0,0,254,0.4)',
        hoverBorderColor: 'rgba(0,0,254,1)',
        data: this.props.averageStatistic[1]
      }
    ]
  })

	componentDidMount() {
    this.interval = setInterval(() => this.setState({ time: Date.now() }), 100);
  }
  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return (
      
      <div>
        <div class="flexbox-container">
        <div>
          </div>
          <div>
            <HorizontalBar data={this.state} />
          </div>
      </div>
      </div>
    );
  }
}

export default HorizontalAverage;